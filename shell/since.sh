#!/bin/sh
set -e

########################################
# variables
########################################
LAST_RELEASE_FILE="./last_release"

#BUILD_FOR_RELEASE="./gradlew testFlightUploadTask"
BUILD_FOR_RELEASE="sh ./buildAndUpload.sh"

########################################
# functions
########################################
function updateLastReleaseHash()
{
	# update file
	echo "$GITHASH2" > $LAST_RELEASE_FILE

	echo "Updated last release hash to $GITHASH2"
}

########################################
# start
########################################
if [[ ! -f $LAST_RELEASE_FILE ]]; then
	echo "Missing $LAST_RELEASE_FILE file - exiting"
	exit 1
fi

# get last commit
echo ""
GITHASH1=$(cat $LAST_RELEASE_FILE)
echo "Last release hash: $GITHASH1"

# current commit
#echo ""
GITHASH2=$(git log -n1 --pretty=oneline | cut -f1 -d' ')
echo "Most recent commit: $GITHASH2"

# get log of changes
echo ""
CHANGES=$(git log --pretty=oneline ${GITHASH1}...${GITHASH2})

if [[ "$GITHASH1" == "$GITHASH2" ]]; then
	echo "No changes between them - exiting"
	exit 1
else
	echo "Changes between them:"
	echo "$CHANGES"
fi

# set for TF plugin
export SOURCE_CODE_CHANGES=$CHANGES

echo ""
echo "Building for release..."
$BUILD_FOR_RELEASE

echo ""
if [[ $? != 0 ]]; then
	echo "Error in build or release (error code $?) - exiting"
	exit 1
else
	echo "Build and release complete - update last release hash? (y/n): "
fi

read UPDATE_HASH_DECISION

# explicit y
if [[ "$UPDATE_HASH_DECISION" == "y" ]]; then
	updateLastReleaseHash

# default
elif [[ "y$UPDATE_HASH_DECISION" == "y" ]]; then
	updateLastReleaseHash

else
	echo "Skipped updating last release hash"
fi

echo "Done."
