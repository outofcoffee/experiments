/**
 * 
 */
package hello8;

/**
 * Replies with a 'Yo' prefix.
 * 
 * @author pete
 * 
 */
public class YoGreeter {

	/**
	 * @param name
	 * @return the informal greeting for the given name
	 */
	public static String greet(String name) {
		return "Yo, " + name;
	}

}
