/**
 * 
 */
package hello8;

/**
 * Greet someone.
 *
 * @author pete
 *
 */
public interface Greeter {
	
	/**
	 * Greet the given person.
	 *
	 * @param name
	 * @return the greeting
	 */
	String greet(String name);
}
