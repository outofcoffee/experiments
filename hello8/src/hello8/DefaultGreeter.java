/**
 * 
 */
package hello8;

/**
 * Default implementation for {@link #answer(String)} uses the
 * {@link YoGreeter}.
 * 
 * @author pete
 * 
 */
public interface DefaultGreeter extends Greeter {

	/**
	 * Default implementation.
	 *
	 * @param question
	 * @return the greeting
	 */
	default String greet(String name) {
		return YoGreeter.greet(name);
	}
}
