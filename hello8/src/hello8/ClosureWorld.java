/**
 * 
 */
package hello8;

import java.util.ArrayList;
import java.util.List;

/**
 * Fun with Java 7 and 8.
 * 
 * @author pete
 * 
 */
public class ClosureWorld {

	/**
	 * Create some {@link Greeter}s, but only print those whose greetings are
	 * informal.
	 * 
	 * @param args
	 *            a single {@link String} should be provided as the name of the person to greet.
	 */
	public static void main(String... args) {
		final String nameToGreet;
		if (args.length == 1) {
			nameToGreet = args[0];
		} else {
			throw new IllegalArgumentException("Usage: " + ClosureWorld.class.getSimpleName() + " <nameToGreet>");
		}

		// diamond
		final List<Greeter> greeters = new ArrayList<>();

		// functional interface
		greeters.add(name -> "Hi, " + name);
		greeters.add(name -> "Hello, " + name);

		// default method
		greeters.add(new DefaultGreeter() {
		});

		// bulk operations
		greeters.stream().filter(greeter -> isInformal(greeter.greet(nameToGreet)))
				.forEach(greeter -> System.out.println(greeter.greet(nameToGreet)));
	}

	/**
	 * Determine if a greeting is informal.
	 * 
	 * @param greeting
	 * @return <code>true</code> if informal, otherwise <code>false</code>.
	 */
	private static boolean isInformal(String greeting) {
		// string switch
		switch (greeting.substring(0, 2)) {
		case "Hi":
		case "Yo":
			return true;
		default:
			return false;
		}
	}
}
