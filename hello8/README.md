# Example using Java 8 and 7 features, like:

- Closures and functional interfaces
- Diamond generics declaration
- Default methods on interfaces
- String switch statements
- Bulk operations like filter and forEach

## Requirements

- Java 8

## How to use

    mkdir bin
    javac src/hello8/*.java -d bin
    cd bin
    java hello8.ClosureWorld Bob

## Output

    Hi, Bob
    Yo, Bob
